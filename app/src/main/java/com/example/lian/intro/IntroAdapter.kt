package com.example.lian.intro

import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.lian.R
import com.example.lian.models.Intro
import kotlinx.android.synthetic.main.custom_intro.view.*

class IntroAdapter(val intros:ArrayList<Intro>):RecyclerView.Adapter<IntroAdapter.Holder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Holder {
       val view=LayoutInflater.from(parent.context).inflate(R.layout.custom_intro,parent,false)
        return Holder(view)
    }

    override fun getItemCount(): Int {
        return intros.size
    }

    override fun onBindViewHolder(holder: Holder, position: Int) {
        holder.itemview.iv_intro.setImageResource(intros.get(position).icon)
        holder.itemview.tv_intro.setText(intros[position].title)
        holder.itemview.setBackgroundColor(Color.parseColor(intros[position].backgrand))
    }







    class Holder(val  itemview: View):RecyclerView.ViewHolder(itemview)


}