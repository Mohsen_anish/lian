package com.example.lian.intro

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.viewpager2.widget.ViewPager2
import com.example.lian.MainActivity
import com.example.lian.R
import com.example.lian.models.Intro
import kotlinx.android.synthetic.main.activity_intro.*
import java.util.*
import kotlin.collections.ArrayList

class IntroActivity : AppCompatActivity() {
    val intros=ArrayList<Intro>()
    lateinit var myadapter:IntroAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)


        setContentView(R.layout.activity_intro)
        val intro1=Intro("مرکز تخصصی کاشت و طراحی ناخن لیان","#ffa801",R.drawable.nailvan)
        val intro2=Intro("کادری مجرب و حرفه ای","#EA2027",R.drawable.nailtow)
        val intro3=Intro("دارای نوبت دهی انلاین","#009432",R.drawable.nailtree)
        intros.addAll(Arrays.asList(intro1,intro2,intro3))
         myadapter=IntroAdapter(intros)
        viewpager.adapter=myadapter
        btn_next.setOnClickListener({
            viewpager.setCurrentItem(viewpager.currentItem+1)
        })
        btn_skip.setOnClickListener({
            val intent=Intent(this,MainActivity::class.java)
            startActivity(intent)
            finish()
        })
        viewpager.registerOnPageChangeCallback(object :ViewPager2.OnPageChangeCallback(){
            override fun onPageSelected(position: Int) {
                super.onPageSelected(position)
                if (position==2){
                    btn_next.visibility=View.GONE
                    btn_skip.setText("شروع برنامه")
                }else{
                    btn_next.visibility=View.VISIBLE
                    btn_skip.setText(" رد کردن")
                }
            }
        })



    }
}