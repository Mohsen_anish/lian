package com.example.lian.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import com.example.lian.entity.Category

@Dao
interface CategoryDao {
    @Insert
    fun insertcategoris(category: Category): Long

    @Query("select * from Category")
    fun getcategory():List<Category>
}