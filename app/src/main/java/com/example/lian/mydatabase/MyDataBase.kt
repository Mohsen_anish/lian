package com.example.lian.mydatabase

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.example.lian.dao.CategoryDao
import com.example.lian.entity.Category

@Database(entities = [Category::class],version = 2)
abstract class MyDataBase : RoomDatabase(){
    abstract fun getCategoryDao():CategoryDao





    companion object {

        @Volatile
        private var instance: MyDataBase? = null
        private val LOCK = Any()

        operator fun invoke(context: Context) = instance ?:
        synchronized(LOCK) {
            instance ?: buildDatabase(context).also {
                instance = it
            }
        }

        private fun buildDatabase(context: Context) =
            Room.databaseBuilder(
                context.applicationContext,
                MyDataBase::class.java,
                "MyDatabase.db"
            )
                .allowMainThreadQueries()
                .fallbackToDestructiveMigration()
                .build()
    }

}