package com.example.lian.entity

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class Category(@PrimaryKey(autoGenerate = true) var id:Int?, val name:String)