package com.example.lian

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.fragment.app.Fragment
import com.example.lian.categoryfragment.CategoryFragment
import com.example.lian.homefragment.HomeFragment
import com.example.lian.nemonehfragment.NemonehFragment
import com.example.lian.profilefragment.ProfileFragment
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        openfragment(HomeFragment())
        bnv.setOnNavigationItemReselectedListener({
            if (it.itemId==R.id.bnv_asli){
                openfragment(HomeFragment())
            }else if (it.itemId==R.id.bnv_category){
                openfragment(CategoryFragment())
            }else if (it.itemId==R.id.bnv_nemoneh){
                openfragment(NemonehFragment())
            }else{
                openfragment(ProfileFragment())
            }
        })
        true

    }
    fun openfragment(fragment: Fragment){
        val trancaction=supportFragmentManager.beginTransaction()
        trancaction.replace(R.id.container_home,fragment)
        trancaction.commit()
    }
}