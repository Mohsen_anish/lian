package com.example.lian.categoryfragment

import android.app.AlertDialog
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.Toast
import com.example.lian.R
import com.example.lian.entity.Category
import com.example.lian.mydatabase.MyDataBase
import com.karumi.dexter.Dexter
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.PermissionDeniedResponse
import com.karumi.dexter.listener.PermissionGrantedResponse
import com.karumi.dexter.listener.PermissionRequest
import com.karumi.dexter.listener.single.PermissionListener
import kotlinx.android.synthetic.main.custom_category.*
import kotlinx.android.synthetic.main.fragment_category.*

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [CategoryFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class CategoryFragment : Fragment() {
    val categoris=ArrayList<Category>()
    lateinit var myadapter:CategoryAdapter

    private var param1: String? = null
    private var param2: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_category, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
         categoris.addAll(MyDataBase.invoke(requireContext()).getCategoryDao().getcategory())
         myadapter= CategoryAdapter(categoris)
        rv_category.adapter=myadapter

        fb_add_category.setOnClickListener({
           val alert=AlertDialog.Builder(requireContext())
            alert.setTitle("نام دسته را وارد نمایید")
            val name=EditText(requireContext())
            name.hint="نام دسته"
            alert.setView(name)
            alert.setPositiveButton("ذخیره",{d,i->
                val newname=name.text.toString()
                val category=Category(null,newname)
                val cat_id=MyDataBase.invoke(requireContext()).getCategoryDao().insertcategoris(category)
                category.id=cat_id.toInt()
                categoris.add(category)
                myadapter.notifyDataSetChanged()
                Toast.makeText(requireContext(),"دسته با موفقیت ثبت شد",Toast.LENGTH_LONG).show()




            })
            alert.setNegativeButton("لغو",{d,i->
                d.dismiss()
            })
            alert.create()
            alert.show()



            
        })

    }



    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment CategoryFragment.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            CategoryFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }
}