package com.example.lian.categoryfragment

import android.net.Uri
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.lian.R
import com.example.lian.entity.Category
import kotlinx.android.synthetic.main.custom_category.view.*

class CategoryAdapter (val categoris:ArrayList<Category>):RecyclerView.Adapter<CategoryAdapter.Holder>(){
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Holder {
      val view=LayoutInflater.from(parent.context).inflate(R.layout.custom_category,parent,false)
        return Holder(view)
    }

    override fun getItemCount(): Int {
        return categoris.size
    }

    override fun onBindViewHolder(holder: Holder, position: Int) {

        holder.itemview.tv_name_category.setText(categoris[position].name)
    }







    class Holder(val itemview:View):RecyclerView.ViewHolder(itemview)


}